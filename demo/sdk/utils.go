package sdk

import (
	"log"
	"path/filepath"
	"time"

	"github.com/avast/retry-go"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/util/homedir"
)

func GetClientset() (*kubernetes.Clientset, error) {
	home := homedir.HomeDir()
	kubeconfigPath := filepath.Join(home, ".kube", "config")

	config, err := clientcmd.BuildConfigFromFlags("", kubeconfigPath)
	if err != nil {
		return nil, err
	}
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		return nil, err
	}

	return clientset, nil

}

// Retry provide retry with default parameters
func Retry(rf retry.RetryableFunc) error {
	return retry.Do(
		rf,
		retry.Attempts(300),
		retry.OnRetry(func(n uint, err error) {
			log.Printf("#%d: %s\n", n, err)
		}),
		retry.DelayType(func(uint, *retry.Config) time.Duration { return 400 * time.Millisecond }),
	)
}
