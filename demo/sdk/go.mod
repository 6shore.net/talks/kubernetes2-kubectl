module gitlab.com/6shore.net/talks/kubernetes2-kubectl/demo/sdk

go 1.12

require (
	github.com/avast/retry-go v2.4.1+incompatible
	github.com/googleapis/gnostic v0.3.1 // indirect
	github.com/imdario/mergo v0.3.7 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/crypto v0.0.0-20190923035154-9ee001bba392 // indirect
	golang.org/x/net v0.0.0-20190921015927-1a5e07d1ff72 // indirect
	golang.org/x/time v0.0.0-20190921001708-c4c64cad1fd0 // indirect
	k8s.io/api v0.0.0-20190920115539-4f7a4f90b2c0
	k8s.io/apimachinery v0.0.0-20190919161714-83fef8059749
	k8s.io/client-go v0.0.0-20190918160344-1fbdaa4c8d90
	k8s.io/klog v1.0.0 // indirect
	k8s.io/utils v0.0.0-20190920012459-5008bf6f8cd6 // indirect
)
