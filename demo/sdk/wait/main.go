package main

import (
	"fmt"

	"gitlab.com/6shore.net/talks/kubernetes2-kubectl/demo/sdk"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"
)

var (
	serviceName = "kad"
	namespace   = "default"
)

func getSvc(cs *kubernetes.Clientset, name string) (*v1.Service, error) {
	svc, err := cs.CoreV1().Services(namespace).Get(name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}

	return svc, nil
}

// checkSvcStatus verify if service is ready to serve traffic
func checkSvcStatus(svc *v1.Service) error {
	// check lb address
	if svc.Spec.Type == v1.ServiceTypeLoadBalancer {
		if ing := svc.Status.LoadBalancer.Ingress; ing == nil || len(ing) == 0 {
			return fmt.Errorf("LoadBalancer does not have public IP yet, have: %+v", ing)
		}
	}

	return nil
}

// checkEndpoints verify service have endpoints
func checkEndpoints(cs *kubernetes.Clientset, name string) error {
	cl := cs.CoreV1().Endpoints(namespace)

	endpoints, err := cl.Get(name, metav1.GetOptions{})
	if err != nil {
		return err
	}
	if len(endpoints.Subsets) == 0 {
		return fmt.Errorf("There are no endpoints for service %s", name)
	}

	return nil
}

// wait for service endpoint health
func main() {
	cs, err := sdk.GetClientset()
	if err != nil {
		panic(err)
	}

	if err := sdk.Retry(func() error {
		// get service
		svc, err := getSvc(cs, serviceName)
		if err != nil {
			return err
		}

		// check status
		if err := checkSvcStatus(svc); err != nil {
			return err
		}

		// check endpoints
		if err := checkEndpoints(cs, svc.ObjectMeta.Name); err != nil {
			return err
		}

		return nil
	}); err != nil {
		panic(err)
	}
}
